# weather-widget

> I searched for Vue.js boilerplate, and i chose the simplest one with webpack included.
> Started to design the interface at first, adding bootstrap, jquery, writing divs, and styling.
> Read some of the official Vue docs to learn how to bind variables to the view and how to implement methods.
> Used jquery to send the request to openweeather API.
> Finalizing the task.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).
